/*Exercise 2-4. Write an alternative version of squeeze(s1,s2) that deletes each character in
s1 that matches any character in the string s2.*/

#include <stdio.h>

void squeeze(char s1[], char s2[]);

int main()
{
    char s1[10] = "asdfasdf"; 
    char s2[3] = "as\0";
    printf("s1: %s\n", s1);
    printf("s2: %s\n", s2);
    squeeze(s1, s2);
    printf("\nresult: %s\n", s1);
    return 0;
}

void squeeze(char s1[], char s2[])
{
    int i, j, k;
    char c;
    k = 0;
    printf("%s\n", s1);
    printf("%s\n", s2);
    while ((c = s2[k++]) != '\0') {
        for (i = j = 0; s1[i] != '\0'; i++) {
            if (s1[i] != c) {
                s1[j++] = s1[i];
            }
        }
        s1[j] = '\0';
        printf("%d s1: %s\n", k, s1);
    }
}