#include <stdio.h>
#define MAXLINE 1000 /* maximum input line length */
#define IN 1 /* inside a blank sequence */
#define OUT 0 /* outside a blank sequence */


/**
Exercise 1 - 22. Write a program to ``fold'' long input lines
into two or more shorter lines after
the last non - blank character that occurs before the n - th column of input.
Make sure your program does something intelligent with very long lines,
and if there are no blanks or tabs before the specified column.
*/

int fold_thresold;

void fold(char s[], char r[]);
int getline(char line[], int maxline);
void copy(char to[], char from[]);
int char_is_blank(char c);
int strlen(char s[]);

int main()
{
	extern int fold_thresold;
	char line[MAXLINE]; /* current input line */
	int len;
	/*
	 printf("Enter folding thresold: ");
	    scanf(" %d", &fold_thresold); */
	fold_thresold = 10;
	int extra_space;

	while ((len = getline(line, MAXLINE)) > 0) {
		/* extra space for line breaks */
		extra_space = 0;
		if (len > fold_thresold) {
			extra_space = (len / fold_thresold) - 1;
			char folded[len];
			fold(line, folded);
			/*printf("%d\n", strlen(line));*/
			/*printf("%d\n", strlen(folded));*/
			printf("%s\n", folded);
		} else {
			printf("%s\n", line);
		}
	}
	return 0;
}

/* getline: read a line into s, return length */
int getline(char s[], int lim)
{
	int c, i;
	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
		s[i] = c;
	}
	if (c == '\n') {
		s[i] = c;
	}
	s[i] = '\0';
	return i;
}

/* copy: copy 'from' into 'to'; assume to is big enough */
void copy(char to[], char from[])
{
	int i;
	i = 0;
	while ((to[i] = from[i]) != '\0')
		++i;
}

void fold(char line[], char folded[]) {
	extern int fold_thresold;
	int i, k, first_blank_position, last_blank_position, state;
	char c;
	i = k = 0;
	last_blank_position = first_blank_position = 0;
	state = OUT;
	while ((c = line[i]) != '\0') {
		if (char_is_blank(c)) {
			if (state == OUT) {
				first_blank_position = i;
			}
			state = IN;
		} else if (state == IN) {
			last_blank_position = i - 1;
			state = OUT;
		}

		if (i > 0 && (((i + 1) % fold_thresold) == 0)) {
			if (state == IN) {
				if (first_blank_position != 0) {
					k = first_blank_position;
					folded[k] = '\n';
					k++;
				}
			} else if (state == OUT) {
				folded[k] = c;
				k++;
				folded[k] = '\n';
			}
		} else {
			folded[k] = c;
		}
		k++;
		i++;
	}
	folded[k] = '\0';
	/*printf("%d\n", i);*/
	/*printf("%d\n", k);*/
}

int char_is_blank(char c) {
	return c == ' ' || c == '\n' || c == '\t';
}

int strlen(char s[]) {
	int i, length;
	i = length = 0;
	while (s[i] != '\0') {
		++length;
		++i;
	}
	return length;
}