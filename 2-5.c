/*
Exercise 2-5. Write the function any(s1,s2), which returns the first location in a string s1
where any character from the string s2 occurs, or -1 if s1 contains no characters from s2.
(The standard library function strpbrk does the same job but returns a pointer to the
location.)
 */

#include <stdio.h>
#include <string.h>

int any(char s1[], char s2[]);

int main()
{
    char s1[10] = "asdfasdfz"; 
    char s2[] = "as";
    char s3[] = "ggfgg";
    char s4[] = "xx";
    char s5[] = "";
    char s6[] = "yuz";
    printf("any %s %s %d\n", s1, s2, any(s1, s2));
    char* p1 = strpbrk(s1, s2);
    if (p1 != NULL)
        printf("strpbrk %s %s %c\n\n", s1, s2, *p1);

    printf("any %s %s %d\n", s1, s3, any(s1, s3));
    p1 = strpbrk(s1, s3);
    if (p1 != NULL)
        printf("strpbrk %s %s %c\n\n", s1, s3, *p1);

    printf("any %s %s %d\n", s1, s4, any(s1, s4));
    p1 = strpbrk(s1, s4);
    if (p1 != NULL)
        printf("strpbrk %s %s %c\n\n", s1, s4, *p1);

    printf("any %s %s %d\n", s1, s5, any(s1, s5));
    p1 = strpbrk(s1, s5);
    if (p1 != NULL)
        printf("strpbrk %s %s %c\n\n", s1, s5, *p1);

    printf("any %s %s %d\n", s1, s6, any(s1, s6));
    p1 = strpbrk(s1, s6);
    if (p1 != NULL)
        printf("strpbrk %s %s %c\n\n", s1, s6, *p1);

    return 0;
}

int any(char s1[], char s2[])
{
    int i, j;
    char c1, c2;
    j = 0;
    while ((c2 = s2[j++]) != '\0') {
        i = 0;
        while ((c1 = s1[i++]) != '\0') {
            if (c1 == c2) {
                return i;
            }
        }
    }
    return -1;
}