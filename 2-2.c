#include <stdio.h>
#define MAXLINE 1000 /* maximum input line length */

void reverse(char s[]);
int getline(char line[], int maxline);
void copy(char to[], char from[]);

/**
 Exercise 1-19. Write a function reverse(s) that reverses the character string s.
 Use it to write a program that reverses its input a line at a time. 
 */

main()
{
	int len;
	char line[MAXLINE]; /* current input line */
	while ((len = getline(line, MAXLINE)) > 0) {
		reverse(line);
		printf("%s\n", line);
	}
}

/* getline: read a line into s, return length */
int getline(char s[], int lim)
{
	int c, i;
	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
		s[i] = c;
	}
	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return i;
}

void reverse(char line[])
{
	int i, k, length;
	i = length = 0;
	while (line[i] != '\0') {
		++length;
		++i;
	}
	char reversed[length];
	i = k = 0;
	for (i = --length; i >= 0; --i)
	{
		reversed[k] = line[i];
		k++;
	}
	reversed[k] = '\0';
	copy(line, reversed);
}

/* copy: copy 'from' into 'to'; assume to is big enough */
void copy(char to[], char from[])
{
	int i;
	i = 0;
	while ((to[i] = from[i]) != '\0')
		++i;
}