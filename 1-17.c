#include <stdio.h>
#define MAXLINE 1000 /* maximum input line length */
#define MAXSTRINGS 100 /* maximum lines in total */
#define THRESOLD 10 
int getline(char line[], int maxline);
void copy(char to[], char from[]);
/* print all input lines that are more than THRESOLD chars */
main()
{
	int len; /* current line length */
	int max; /* maximum length seen so far */
	int i, j;
	char line[MAXLINE]; /* current input line */
	char longest[MAXLINE]; /* longest line saved here */
	char strings[MAXSTRINGS][MAXLINE];
	max = 0;
	i = 0;
	while ((len = getline(line, MAXLINE)) > 0) {
		if (len > max) {
			max = len;
		}
		if (len > THRESOLD) {
			copy(strings[i], line);
			i++;
		}
	}
	if (max > 0) /* there was a line */ {
		printf("%s\n", "string more than 10 chars:");
		for (j = 0; j < i; ++j) {
			printf("%s", strings[j]);	
		}
	}
	return 0;
}
/* getline: read a line into s, return length */
int getline(char s[], int lim)
{
	int c, i;
	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
		s[i] = c;
	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return i;
}
/* copy: copy 'from' into 'to'; assume to is big enough */
void copy(char to[], char from[])
{
	int i;
	i = 0;
	while ((to[i] = from[i]) != '\0')
		++i;
}
