/*
Exercise 2-3. Write a function htoi(s), which converts a string
of hexadecimal digits
(including an optional 0x or 0X) into its equivalent integer value.
The allowable digits are 0
through 9, a through f, and A through F.
 */
#include <stdio.h>
#include <assert.h>

#define ASCII_HEX_DIFFERENCE 87

int htoi(char s[]);
int is_valid_hex_char(char c);
int hex_char_to_int(char c);
int power(int m, int n);
void test(void);

int main()
{
	test();

	printf("%d = 59dec\n", htoi("3B") );
	printf("%d = 59305dec\n", htoi("E7A9") );
	printf("%d = 16dec\n\n", htoi("10") );

	printf("%d = 59dec\n", htoi("0x3B") );
	printf("%d = 59305dec\n", htoi("0xE7A9") );
	printf("%d = 16dec\n\n", htoi("0x10") );

	printf("%d = 59dec\n", htoi("0X3B") );
	printf("%d = 59305dec\n", htoi("0XE7A9") );
	printf("%d = 16dec\n", htoi("0X10") );
	return 0;
}


void test()
{
	assert(10 == hex_char_to_int('A'));
	assert(11 == hex_char_to_int('B'));
	assert(12 == hex_char_to_int('C'));
	assert(13 == hex_char_to_int('D'));
	assert(14 == hex_char_to_int('E'));
	assert(15 == hex_char_to_int('F'));
	assert(10 == hex_char_to_int('a'));
	assert(11 == hex_char_to_int('b'));
	assert(12 == hex_char_to_int('c'));
	assert(13 == hex_char_to_int('d'));
	assert(14 == hex_char_to_int('e'));
	assert(15 == hex_char_to_int('f'));
	assert(is_valid_hex_char('a'));
	assert(is_valid_hex_char('b'));
	assert(is_valid_hex_char('c'));
	assert(is_valid_hex_char('d'));
	assert(is_valid_hex_char('e'));
	assert(is_valid_hex_char('f'));
	assert(is_valid_hex_char('A'));
	assert(is_valid_hex_char('B'));
	assert(is_valid_hex_char('C'));
	assert(is_valid_hex_char('D'));
	assert(is_valid_hex_char('E'));
	assert(is_valid_hex_char('F'));
	assert(is_valid_hex_char('1'));
	assert(is_valid_hex_char('2'));
	assert(is_valid_hex_char('3'));
	assert(is_valid_hex_char('4'));
	assert(is_valid_hex_char('5'));
	assert(is_valid_hex_char('6'));
	assert(is_valid_hex_char('7'));
	assert(is_valid_hex_char('8'));
	assert(is_valid_hex_char('9'));
	assert(!is_valid_hex_char('Y'));
	assert(!is_valid_hex_char('G'));
	assert(!is_valid_hex_char('Z'));
}

int htoi(char s[])
{
	int i, j, total, num, cond;
	char c;

	i = j = total = num = 0;

	/* getting length */
	while ((c = s[i]) != '\0') {
		i++;
	}
	i -= 1;

	if (s[0] == '0' && (s[1] == 'x' || s[1] == 'X')) {
		cond = 2;
	} else {
		cond = 0;
	}

	while (i >= cond)
	{
		c = s[i];
		if (is_valid_hex_char(c)) {
			num = hex_char_to_int(c);
			total += num * power(16, j);
		} else {
			return -1;
		}
		j++;
		i--;
	}

	return total;
}

int is_valid_hex_char(char c)
{
	return (c >= '0' && c <= '9')
	       || (c >= 'a' && c <= 'f')
	       || (c >= 'A' && c <= 'F');
}

int hex_char_to_int(char c)
{
	if (c >= '0' && c <= '9') {
		return c-'0';
	} else {
		if (c >= 'A' && c <= 'F') {
			c = c + 'a' - 'A';
		}
		return c-ASCII_HEX_DIFFERENCE;
	}
}

/* power: raise base to n-th power; n >= 0 */
int power(int base, int n)
{
	int i, p;
	p = 1;
	for (i = 1; i <= n; ++i)
		p = p * base;
	return p;
}