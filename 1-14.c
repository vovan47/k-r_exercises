#include <stdio.h>
/* Exercise 1-14. Write a program to print a 
histogram of the frequencies of different characters
in its input.  */
#define MAX_CHARS 128
main()
{
	int c, i, j, nwhite, nother;
	int ndigit[10];
	int ascii_chars[MAX_CHARS];
	nwhite = nother = 0;
	for (i = 0; i < 10; ++i) {
		ndigit[i] = 0;
	}
	for (i = 0; i < 128; ++i) {
		ascii_chars[i] = 0;
	}
	while ((c = getchar()) != EOF) {
		if (c >= '0' && c <= '9') { 
			++ndigit[c-'0'];
		} else if (c == ' ' || c == '\n' || c == '\t') {
			++nwhite;
		} else if (c > 0 && c < 128) {
			++ascii_chars[c];
		} else {
			++nother;
		}
	}
	printf("chars =\n");
	for (i = 0; i < MAX_CHARS; ++i) {
		if (ascii_chars[i] == 0) continue;	
		printf("%c(%d)\t|", i, i);
		for (j = 0; j < ascii_chars[i]; ++j) {
			printf("#");
		}
		printf("\n");
	}
}